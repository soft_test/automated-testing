const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet P in password', () => {
    expect(checkAlphabet('P')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has Digit 1234 in password', () => {
    expect(checkDigit('1234')).toBe(true)
  })
  test('should has not Digit Oom! in password', () => {
    expect(checkDigit('Oom!')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has Symbol ! in password', () => {
    expect(checkSymbol('!11')).toBe(true)
  })
})

describe('Test Password', () => {
  test('should Password oom070841# in password', () => {
    expect(checkPassword('oom070841#')).toBe(true)
  })
  test('should Password oom07# in password', () => {
    expect(checkPassword('oom07#')).toBe(false)
  })
  test('should Password oom070841 in password', () => {
    expect(checkPassword('oom070841')).toBe(false)
  })
  test('should Password oomonanong# in password', () => {
    expect(checkPassword('oomonanong#')).toBe(false)
  })
})
